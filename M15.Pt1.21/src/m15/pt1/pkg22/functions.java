/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pt1.pkg22;

/**
 *
 * @author marioalonso
 */
public class functions {

    /**
     * Description: this method does the reverse of a String
     *
     * @param seq
     * @return reverse
     */
    public static String ReverseADN(String seq) {
        String reverse = new StringBuffer(seq).reverse().toString();
        
        return reverse;
    }

    /**
     * Description: This method calculates the maximum and minimum bases
     *
     * @param seq
     * @param maxmin
     * @return X,Y
     */
    public static String findBaseCount(String seq, String maxmin) {
        String[] arraySeq = seq.split("");
        int A = 0;
        int C = 0;
        int G = 0;
        int T = 0;

        for (int i = 0; i < arraySeq.length; i++) {
            if (arraySeq[i].equals("A")) {
                A++;
            } else if (arraySeq[i].equals("C")) {
                C++;
            } else if (arraySeq[i].equals("T")) {
                T++;
            } else if (arraySeq[i].equals("G")) {
                G++;
            }
        }

        String X;
        if (A > G && A > C && A > T) {
            X = "A";
        } else {
            if (G > A && G > C && G > T) {
                X = "G";
            } else {
                if (C > A && C > G && C > T) {
                    X = "C";
                } else {
                    X = "T";
                }
            }
        }

        String Y;
        if (A < G && A < C && A < T) {
            Y = "A";
        } else {
            if (G < A && G < C && G < T) {
                Y = "G";
            } else {
                if (C < A && C < G && C < T) {
                    Y = "C";
                } else {
                    Y = "T";
                }
            }
        }

        if (maxmin == "max") {
            return X;
        } else {
            return Y;
        }
    }

    /**
     * Description: This method count the total Bases
     *
     * @param seq
     * @return count
     */
    public static int countBases(String seq) {

        int count = 0;

        for (int i = 0; i < seq.length(); i++) {
            count++;
        }

        return count;
    }

}
