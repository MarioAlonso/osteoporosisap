package m15.pt1.pkg22;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class M15Pt122 {

    public static void main(String[] args) throws FileNotFoundException, IOException {


        
        String cadena = ADNFileReader.fileReader();

        int option = 0;

        do {
            option = showMenu();
            switch (option) {
                case 1:
                    String reverse = functions.ReverseADN(cadena);
                    System.out.println("Reverse Sequence: " + reverse);
                    break;
                case 2:
                    String max = functions.findBaseCount(cadena, "max");
                    System.out.println("The most repeated base is: " + max);
                    break;
                case 3:
                    String min = functions.findBaseCount(cadena, "min");
                    System.out.println("The less repeated base is: " + min);
                    break;
                case 4:
                    int count = functions.countBases(cadena);
                    System.out.println("The number of bases is: " + count);
                    break;
                default:
                    System.out.println("Bye!");
                    break;

            }
        } while (option != 0 && option <= 4);

    }

    private static int showMenu() {

        Scanner sc = new Scanner(System.in);
        int option = -1;

        System.out.println("MENU");
        System.out.println("0. Exit");
        System.out.println("1. Reverse the sequence");
        System.out.println("2. Find the most repeated base");
        System.out.println("3. Find the less repeated base");
        System.out.println("4. Count all the bases");
        System.out.print("Choose an option: ");
        option = sc.nextInt();

        return option;
    }
}
