/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package m15.pt1.pkg22;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author marioalonso
 */
public class ADNFileReader {
    
    

    /**
     * Description: Method that reads the file Sequence.txt and returns 
     * the content in a String
     * @return cadena
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public static String fileReader() throws FileNotFoundException, IOException{
    String cadena;
    FileReader f = new FileReader("/home/marioalonso/Sequence.txt");
    BufferedReader b = new BufferedReader(f);
    cadena  = b.readLine();
    System.out.println ("Sequence: " + cadena);

    b.close ();
    
    return cadena;
 
    }
    
}
